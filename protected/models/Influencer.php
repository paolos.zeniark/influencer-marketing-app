<?php

/**
 * This is the model class for table "{{Influencer}}".
 *
 * The followings are the available columns in table '{{Influencer}}':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $seeding_platform
 * @property integer $followers
 * @property string $handle_tiktok
 * @property string $handle_instagram
 * @property string $handle_youtube
 * @property integer $following_tier
 * @property integer $demographic
 * @property integer $segment
 * @property string $notes
 * @property integer $added_by_user_id
 * @property string $date_created
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property User $addedByUser
 */
class Influencer extends CActiveRecord
{
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;

	const SEEDING_PLATFORM_TIKTOK = 1;
	const SEEDING_PLATFORM_INSTAGRAM = 2;
	const SEEDING_PLATFORM_YOUTUBE = 3;
	const SEEDING_PLATFORM_TWITTER = 4;
	const SEEDING_PLATFORM_FACEBOOK = 5;

	const FOLLOWING_TIER_MICRO = 1;
	const FOLLOWING_TIER_MACRO = 2;

	const DEMOGRAPHIC_GENZ = 1;
	const DEMOGRAPHIC_MILLENIAL = 2;

	const SEGMENT_BEAUTY = 1;
	const SEGMENT_SKINFLUENCER = 2;
	const SEGMENT_LIFESTYLE = 3;
	const SEGMENT_PERSONALITY = 4;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{influencer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, seeding_platform, followers, following_tier, demographic, segment, added_by_user_id, notes', 'required'),
			array('seeding_platform, followers, following_tier, demographic, segment, added_by_user_id', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, handle_tiktok, handle_instagram, handle_youtube', 'length', 'max'=>128),
			array('notes, hyperlink_tiktok, hyperlink_instagram, hyperlink_youtube', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, seeding_platform, followers, handle_tiktok, handle_instagram, handle_youtube, hyperlink_tiktok, hyperlink_instagram, hyperlink_youtube, following_tier, demographic, segment, notes, added_by_user_id, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addedByUser' => array(self::BELONGS_TO, 'User', 'added_by_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'seeding_platform' => 'Seeding Platform',
			'followers' => 'Followers',
			'handle_tiktok' => 'Tiktok Handle',
			'handle_instagram' => 'Instagram Handle ',
			'handle_youtube' => 'Youtube Handle',
			'hyperlink_tiktok' => 'Tiktok Hyperlink',
			'hyperlink_instagram' => 'Instagram Hyperlink',
			'hyperlink_youtube' => 'Youtube Hyperlink',
			'following_tier' => 'Following Tier',
			'demographic' => 'Demographic',
			'segment' => 'Segment',
			'notes' => 'Notes',
			'added_by_user_id' => 'Added By User',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'full_name' => 'Full Name'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('seeding_platform',$this->seeding_platform);
		$criteria->compare('followers',$this->followers);
		$criteria->compare('handle_tiktok',$this->handle_tiktok,true);
		$criteria->compare('handle_instagram',$this->handle_instagram,true);
		$criteria->compare('handle_youtube',$this->handle_youtube,true);
		$criteria->compare('following_tier',$this->following_tier);
		$criteria->compare('demographic',$this->demographic);
		$criteria->compare('segment',$this->segment);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('added_by_user_id',$this->added_by_user_id);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Influencer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes() 
	{
		return [
			'notDeleted' => [
				'condition' => 'status != ' . self::STATUS_DELETED
			]
		];
	}

	public static function getSeedingPlatformList() 
	{
		return [
			self::SEEDING_PLATFORM_TIKTOK => 'TikTok',
			self::SEEDING_PLATFORM_INSTAGRAM => 'Instagram',
			self::SEEDING_PLATFORM_YOUTUBE => 'Youtube',
			// self::SEEDING_PLATFORM_TWITTER => 'Twitter',
			// self::SEEDING_PLATFORM_FACEBOOK => 'Facebook',
		];
	}

	public static function getSeedingPlatformLabel($key) {
		return self::getSeedingPlatformList()[$key];
	}

	public static function getFollowingTierList() 
	{
		return [
			self::FOLLOWING_TIER_MICRO => 'Micro',
			self::FOLLOWING_TIER_MACRO => 'Macro',
		];
	}

	public static function getFollowingTierLabel($key) {
		return self::getFollowingTierList()[$key];
	}

	public static function getDemographicList() 
	{
		return [
			self::DEMOGRAPHIC_GENZ => 'Gen Z',
			self::DEMOGRAPHIC_MILLENIAL => 'Millenial',
		];
	}

	public static function getDemographicLabel($key) {
		return self::getDemographicList()[$key];
	}

	public static function getSegmentList() 
	{
		return [
			0 => 'Animals (C)',
			1 => 'Animation',
			2 => 'Anime / Weeb',
			3 => 'Arts and Crafts (C)',
			4 => 'Beauty',
			5 => 'Caption Content',
			6 => 'Career Advice',
			7 => 'Challenges / Gimmick',
			8 => 'Comedy',
			9 => 'Commentary (C)',
			10 => 'Content w/ Friends',
			11 => 'Cosplay (C)',
			12 => 'Couples',
			13 => 'Dance',
			14 => 'DIY, Home Renovation, Interior Design',
			15 => 'Educational (C)',
			16 => 'Family (C)',
			17 => 'Fashion (C)',
			18 => 'Finance (C)',
			19 => 'Fitness and Diet',
			20 => 'Food',
			21 => 'Gamer (C)',
			22 => 'Horror',
			23 => 'Interview',
			24 => 'Kpop / Koreaboo',
			25 => 'LGBTQ+',
			26 => 'Lifestyle',
			27 => 'Lip Sync',
			28 => 'Love & Dating',
			29 => 'Martial Arts, Extreme Sports, and Tricks (C)',
			30 => 'Mixology (Drinks)',
			31 => 'Music (C)',
			32 => 'Niche Hobbies (C)',
			33 => 'Personality',
			34 => 'Podcasts',
			35 => 'POV',
			36 => 'Pranks',
			37 => 'Professionals (C)',
			38 => 'Psychology and Mental Health',
			39 => 'Reaction Vids',
			40 => 'School/Student Life & Advice',
			41 => 'Sensitive Topics (C)',
			42 => 'SFX Makeup',
			43 => 'SFX Visual effects',
			44 => 'Shopping Hauls / Unboxing',
			45 => 'Skits & Roleplay (C)',
			46 => 'Skinfluencer',
			47 => 'Sports (C)',
			48 => 'Style Transformation',
			49 => 'Survival /Outdoor',
			50 => 'Technology',
			51 => 'Thirst Trap',
			52 => 'Tiktok Trends / Challenges Videos',
			53 => 'Transition',
			54 => 'Travel and Sightseeing',
			55 => 'Vlogs / Storytime / Monologues',
		];
	}

	public static function getSegmentLabel($key) {
		return self::getSegmentList()[$key];
	}

	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
			if ($this->isNewRecord) 
			{
				$this->date_created = date('Y-m-d H:i:s');
				$this->date_updated = date('Y-m-d H:i:s');
			}
			else 
			{
				$this->date_updated = date('Y-m-d H:i:s');
			}
		}

		return true;
	}

	public function hasDuplicateHandle($platform) 
	{
		$criteria = new CDbCriteria;

		$condition = '';

		switch ($platform) {
			case self::SEEDING_PLATFORM_TIKTOK:
				$criteria->mergeWith([
					'condition' => 'handle_tiktok = :handle_tiktok',
					'params' => [':handle_tiktok' => trim($this->handle_tiktok)]
				]);
			break;

			case self::SEEDING_PLATFORM_INSTAGRAM:
				$criteria->mergeWith([
					'condition' => 'handle_instagram = :handle_instagram',
					'params' => [':handle_instagram' => trim($this->handle_instagram)]
				]);
			break;

			case self::SEEDING_PLATFORM_YOUTUBE:
				$criteria->mergeWith([
					'condition' => 'handle_youtube = :handle_youtube',
					'params' => [':handle_youtube' => trim($this->handle_youtube)]
				]);
			break;
		}

		if (!$this->isNewRecord)
		{
			$criteria->mergeWith([
				'condition' => 'id <> :id',
				'params' => [':id' => $this->id]
			]);
		}

		$influencers = self::model()->notDeleted()->findAll($criteria);

		return $influencers && count($influencers) > 0;
	}

	public function hasDuplicateFullname()
	{
		$firstName = trim(strtolower($this->first_name));
		$lastName = trim(strtolower($this->last_name));

		$criteria = new CDbCriteria;

		$criteria->mergeWith([
			'condition' => 'LOWER(first_name) = :first_name and LOWER(last_name) = :last_name',
			'params' => [':first_name' => $firstName, ':last_name' => $lastName]
		]);

		if (!$this->isNewRecord)
		{
			$criteria->mergeWith([
				'condition' => 'id <> :id',
				'params' => [':id' => $this->id]
			]);
		}

		$influencer = self::model()->notDeleted()->findAll($criteria);

		return $influencer && count($influencer) > 0;
	}
}
