<?php
/* @var $this InfluencerController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile('https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css');
Yii::app()->clientScript->registerScriptFile('https://code.jquery.com/jquery-3.6.0.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('https://code.jquery.com/ui/1.13.1/jquery-ui.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('general', "

$(document).ready(function() {
	var windowHeight = $(window).height();
	var panelHeight = windowHeight*0.78;

	$('.manage .panel-body').css({height: panelHeight+'px'});

	$('#date_created_from, #date_created_to').datepicker({dateFormat: 'yy-mm-dd'});
});

");

?>

<style>
.container {width: 90% !important;}
.summary {text-align: left !important;}
.pager {text-align: left !important;}
</style>

<div class="container">
	<div class="col-md-12">
		<?php $this->widget('Flash', array(
			'flashes' => Yii::app()->user->getFlashes()
		)); ?>
	</div>
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span><strong>Filter</strong></span>
			</div>
			<div class="panel-body">
				<?php echo CHtml::beginForm('index', 'GET'); ?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('created') . ' <em>(From)</em>', 'date_created_from'); ?>
								<?php echo CHtml::textField('date_created_from', $filterParams['date_created_from'], ['class' => 'form-control input-sm', 'autocomplete' => 'off']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('created') . ' <em>(To)</em>', 'date_created_from'); ?>
								<?php echo CHtml::textField('date_created_to', $filterParams['date_created_to'], ['class' => 'form-control input-sm', 'autocomplete' => 'off']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('added_by'), 'added_by'); ?>
								<?php echo CHtml::dropDownList('added_by_user_id', $filterParams['added_by_user_id'], CHtml::listData(User::model()->findAll(), 'id', 'username'), ['class' => 'form-control input-sm', 'empty' => 'All']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('full_name'), 'full_name'); ?>
								<?php echo CHtml::textField('full_name', $filterParams['full_name'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('seeding_platform'), 'seeding_platform'); ?>
								<?php echo CHtml::dropDownList('seeding_platform', $filterParams['seeding_platform'], Influencer::getSeedingPlatformList(), ['class' => 'form-control input-sm', 'empty' => 'All']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('followers') . ' (<em>less than or equal</em>)', 'followers'); ?>
								<?php echo CHtml::textField('followers', $filterParams['followers'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('handle_tiktok'), 'handle_tiktok'); ?>
								<?php echo CHtml::textField('handle_tiktok', $filterParams['handle_tiktok'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('handle_instagram'), 'handle_instagram'); ?>
								<?php echo CHtml::textField('handle_instagram', $filterParams['handle_instagram'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('handle_youtube'), 'handle_youtube'); ?>
								<?php echo CHtml::textField('handle_youtube', $filterParams['handle_youtube'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('following_tier'), 'following_tier'); ?>
								<?php echo CHtml::dropDownList('following_tier', $filterParams['following_tier'], Influencer::getFollowingTierList(), ['class' => 'form-control input-sm', 'empty' => 'All']); ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('demographic'), 'demographic'); ?>
								<?php echo CHtml::dropDownList('demographic', $filterParams['demographic'], Influencer::getDemographicList(), ['class' => 'form-control input-sm', 'empty' => 'All']); ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('segment'), 'segment'); ?>
								<?php echo CHtml::dropDownList('segment', $filterParams['segment'], Influencer::getSegmentList(), ['class' => 'form-control input-sm', 'empty' => 'All']); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<?php echo CHtml::label($model->getAttributeLabel('notes'), 'notes'); ?>
								<?php echo CHtml::textField('notes', $filterParams['notes'], ['class' => 'form-control input-sm']); ?>
							</div>
						</div>
						<div class="col-md-12 text-right">
							<?php echo CHtml::link('Clear', ['influencer/index'], ['class' => 'btn btn-default']); ?>
							<?php echo CHtml::submitButton('Submit', ['class' => 'btn btn-primary']); ?>
						</div>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-default manage">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-6">
						<span><strong>Manage</strong></span>
					</div>
					<div class="col-md-6 text-right">
						<?php echo CHtml::link('<span class="glyphicon glyphicon-cloud-download"></span>', Yii::app()->createUrl('influencer/export').'?'.http_build_query($filterParams)); ?>
					</div>
				</div>
			</div>
			<div class="panel-body" style="overflow: scroll;">
				<div style="width: 2000px;">
					<?php $this->widget('zii.widgets.CListView', array(
						'id' => 'influencer-list-view',
						'dataProvider' => $influencersDP,
						'itemView' => '_view',
						'template' => '
							{summary}
							<table class="table table-hover table-condensed"">
								<thead>
									<tr class="active">
										<th>#</th>
										<th>Date Created</th>
										<th>Added By</th>
										<th>Full Name</th>
										<th>Seeding Plaform</th>
										<th>Followers</th>	
										<th>TikTok Handle</th>
										<th>Instgram Handle</th>
										<th>Youtube Handle</th>
										<th>Following Tier</th>
										<th>Demographic</th>
										<th>Segment</th>
										<th>Notes</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									{items}
								</tbody>
							</table>
							{pager}
						',
						'emptyText' => '<tr><td colspan="14">No result found</td></tr>'
					)); ?>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
