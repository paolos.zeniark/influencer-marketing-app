<?php
/* @var $this InfluencerController */
/* @var $model Influencer */
?>

<div class="container">
	<div class="col-md-offset-2 col-md-8">
		<div class="panel panel-default form">
			<div class="panel-heading"><strong>Update</strong></div>
			<div class="panel-body">
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</div>